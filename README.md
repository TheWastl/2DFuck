# 2DFuck

2DFuck is an esoteric programming language similar to brainfuck. [Here](https://tio.run/#2dfuck) is an online interpreter (thanks Dennis!)

## Memory model

- The **accumulator** is one bit of data, initialised to zero.
- The **field** is a 2D-Array of bits, each initialised to zero. It is infinite in all directions.
- The **field pointers X and Y** are two integers used to index the field, both initialised to zero

The **current cell** is a bit in the field, at position (X|Y) (`field[X][Y]`).

## Instructions

- `^` decrements the **field pointer Y** (`Y--`)
- `>` increments the **field pointer X** (`X++`)
- `v` increments the **field pointer Y** (`Y++`)
- `<` decrements the **field pointer X** (`X--`)
- `!` flips the **accumulator** (`acc = !acc`)
- `r` puts the bit in the **current cell** into the **accumulator** (`acc = field[X][Y]`)
- `x` takes the XOR of the **accumulator** and the **current cell** and stores it in the **current cell** (`field[X][Y] ^^= acc`)
- `,` reads the next bit of input and stores it in the **accumulator**. Each character has 8 bits, highest bit first.
- `.` writes the **accumulator** bit to output. Each character has 8 bits, highest bit first.
- `l` executes one step of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) on the **field** and stores the result in the **field**
- `[...]` executes `...` as long as the **accumulator** is 1 (`while(acc) { ... }`)
- `?` writes the accumulator and the field with a mark at the current cell to STDERR.

## Debugging

You can turn on debugging with the `-d` switch. If debugging is on, you get information about every executed command.

## Computational class

2DFuck is Turing complete:

    brainfuck -> 2DFuck
    BEGIN     -> !x
    <         -> ^^r!xr
    >         -> vvr!xr
    .         -> v>r.>r.>r.>r.>r.>r.>r.>r.^r![<r!]!
    ,         -> v>rx,x>rx,x>rx,x>rx,x>rx,x>rx,x>rx,x>rx,x^r![<r!]!
    +         -> v>>>>>>>>xr![<xr!]^r![<r!]vrx^r
    -         -> vx>>>>>>>>xr[<xr]^r![<r!]vrx^r
    [         -> vx>>>>>>>>r![<r!]^r![[<r!]!vx^
    ]         -> vx>>>>>>>>r![<r!]^r!]v!x^
